import React from "react";
import "./input.css";

export const Input = (props) => {
  return <div className="input">{props.input}</div>;
};
// export class Input extends Component {
//     render() {
//       return (
//         <div className='input'>{this.props.input}</div>
//       );
//     }
//   }
