import "./Button.css";
import React from "react";


const Button=(props)=>{
 const isOperator = (val) => {
    return !isNaN(val) || val === "." || val === "=";
  };
  return(
    <div
        className={`button-wrapper ${
          isOperator(props.children) ? null : "operator"
        }`} onClick={()=>props.handleClick(props.children)}
      >
        {" "}
        {props.children}{" "}
      </div>
  )
}

// class Button extends Component {
//   isOperator = (val) => {
//     return !isNaN(val) || val === "." || val === "=";
//   };
//   render() {
//     return (
//       <div
//         className={`button-wrapper ${
//           this.isOperator(this.props.children) ? null : "operator"
//         }`} onClick={()=>this.props.handleClick(this.props.children)}
//       >
//         {" "}
//         {this.props.children}{" "}
//       </div>
//     );
//   }
// }



export default Button;
