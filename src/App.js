import React, { Component } from "react";
import "./App.css";
import Button from "./components/Button";
import { Input } from "./components/Input";
import { ClearButton } from "./components/ClearButton";
import { Header } from "./components/header";


import * as math from "mathjs";

class App extends Component {
  state = {
    input: "",
  };

  addToInput = (val) => {
    this.setState({ input: this.state.input + val });
  };

  handleEqual = () => {
    // if (
    //   this.state.input === null ||
    //   this.state.input === undefined ||
    //   this.state.input[this.state.input.length-1].includes('+') ||
    //   this.state.input[this.state.input.length-1].includes('-') ||
    //   this.state.input[this.state.input.length-1].includes('*') ||
    //   this.state.input[this.state.input.length-1].includes('/') ||
    //   this.state.input[this.state.input.length-1].includes('.') ||

    //   this.state.input.includes("++") ||
    //   this.state.input.includes("--") ||
    //   // this.state.input.includes("//") ||
    //   this.state.input.includes("**") ||
    //   this.state.input.includes("*/") ||
    //   this.state.input.includes("/*") ||
    //   this.state.input.includes("+-") ||
    //   this.state.input.includes("-+") ||
    //   this.state.input.includes("+/") ||
    //   this.state.input.includes("/+") ||
    //   this.state.input.includes("-/") ||
    //   this.state.input.includes("/-") ||
    //   this.state.input.includes("Enter a valid expression") ||
    //   this.state.input.includes("+*") ||
    //   this.state.input.includes("..") ||
    //   this.state.input.includes("*+") ||
    //   this.state.input.includes("undefined")

    //  || this.state.input.length === 1
    // ) {
    //   return this.setState({ input: "Enter a valid expression" });
    // }
    try {
      this.setState({ input: math.evaluate(this.state.input) });
    } catch (error) {
      this.setState({ input: 'Enter a valid expression' });
    }
  };
  render() {
    return (

        <div className="app">
          <div className="container">
            <div className="row">
              <Header />
            </div>
            <Input input={this.state.input}></Input>
            <div className="row">
              <Button handleClick={this.addToInput}>7</Button>
              <Button handleClick={this.addToInput}>8</Button>
              <Button handleClick={this.addToInput}>9</Button>
              <Button className="button" handleClick={this.addToInput}>
                /
              </Button>
            </div>

            <div className="row">
              <Button handleClick={this.addToInput}>4</Button>
              <Button handleClick={this.addToInput}>5</Button>
              <Button handleClick={this.addToInput}>6</Button>
              <Button handleClick={this.addToInput}>*</Button>
            </div>
            <div className="row">
              <Button handleClick={this.addToInput}>1</Button>
              <Button handleClick={this.addToInput}>2</Button>
              <Button handleClick={this.addToInput}>3</Button>
              <Button handleClick={this.addToInput}>+</Button>
            </div>
            <div className="row">
              <Button handleClick={this.addToInput}>.</Button>
              <Button handleClick={this.addToInput}>0</Button>
              <div className="equalTo">
                <Button id="equal" handleClick={() => this.handleEqual()}>
                  =
                </Button>
              </div>
              <Button handleClick={this.addToInput}>-</Button>
            </div>
            <div className="row">
              <ClearButton handleClear={() => this.setState({ input: "" })}>
                Clear
              </ClearButton>
            </div>
          </div>
        </div>

    );
  }
}

export default App;
